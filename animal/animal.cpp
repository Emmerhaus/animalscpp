﻿#include <iostream>
#include <vector>


class Animal {
public:
    virtual void Voice() = 0;
};

class Dog : public Animal {
public:
    void Voice() override {
        std::cout << "Woof\n";
    }
};

class Cat : public Animal {
public:
    void Voice() override {
        std::cout << "mya\n";
    }
};

class Elephant : public Animal {
public:
    void Voice() override {
        std::cout << "oooooo\n";
    }
};


int main()
{
    std::vector<Animal*> v;
    for (int i = 0; i < 30; ++i) {
        if (i % 3 == 0) {
            v.push_back(new Dog);
        }
        else if (i % 3 == 1) {
            v.push_back(new Cat);
        }
        else if (i % 3 == 2) {
            v.push_back(new Elephant);
        }
    }

    for (auto a : v) {
        a->Voice();
    }
    
    for (auto a : v) {
        delete a;
    }
}